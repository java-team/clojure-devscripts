# (C) 2010 Peter Collingbourne
# Description: Clojure helper makefile for CDBS
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License as
# published by the Free Software Foundation; either version 2, or (at
# your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
# 02111-1307 USA.

_cdbs_scripts_path ?= /usr/lib/cdbs
_cdbs_rules_path ?= /usr/share/cdbs/1/rules
_cdbs_class_path ?= /usr/share/cdbs/1/class

ifndef _cdbs_class_clojure
_cdbs_class_clojure = 1

DEB_CLOJURE_JAR = $(shell readlink /usr/share/java/clojure.jar)
DEB_CLOJURE_JAR_PATH = /usr/share/java/$(DEB_CLOJURE_JAR)

DEB_CLOJURE_PKG = $(shell dpkg -S $(DEB_CLOJURE_JAR_PATH) | cut -d: -f1)

DEB_CLOJURE_VER = $(subst clojure,,$(DEB_CLOJURE_PKG))

DEB_CLOJURE_MAVEN_RULE = "* clojure * s/.*/$(DEB_CLOJURE_VER).x/"

DEB_CLOJURE_VERSIONED_NAME = $(patsubst %-clj,%,$(1))-clj$(DEB_CLOJURE_VER)

DEB_CLOJURE_USE ?= maven

ifeq ($(DEB_CLOJURE_USE),maven)

include $(_cdbs_class_path)/maven.mk$(_cdbs_makefile_suffix)

DEB_PATCHPOMS_ARGS += --extra-rule=$(DEB_CLOJURE_MAVEN_RULE)

endif

$(patsubst %,install/%,$(DEB_ALL_PACKAGES)) ::
	echo clojure:Version=$(DEB_CLOJURE_VER) >> debian/$(cdbs_curpkg).substvars
	echo clojure:Provides=$(call DEB_CLOJURE_VERSIONED_NAME,$(cdbs_curpkg)) >> debian/$(cdbs_curpkg).substvars

endif
